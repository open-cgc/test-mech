from ..domains.domains_pb2 import GameState

def parse_game_state(data):
    game_state = GameState()
    game_state.ParseFromString(data)
    return game_state

def serialize_moving(moving):
    data = moving.SerializeToString()
    return data
