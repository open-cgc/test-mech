import sys

from ..domains.domains_pb2 import Moving

def update(strategy, game_state):
    me = game_state.me
    world = game_state.world
    moving = Moving()
    strategy.move(me, world, moving)
    return moving
