class Colors:
    black = 0, 0, 0
    white = 200, 200, 200
    gray = 50, 50, 50
    green = 100, 150, 100
    red = 150, 100, 100
    blue = 50, 90, 150


def draw(world, screen, pygame):
    screen.fill(Colors.blue)
    for player in world.players:
        pygame.draw.circle(screen, Colors.green, (player.x, player.y), 5)
