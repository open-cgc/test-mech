import argparse
import sys

from . import default_strategy
from .default_strategy.main import update
from .default_strategy.network import Network
from .default_strategy.my_strategy import Strategy

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', metavar='port', type=int,
                        default=12345, help='Network port')

    args = parser.parse_args()

    port = args.port

    network = Network(port)
    strategy = Strategy()
    while True:
        game_state = network.get_game_state()
        moving = update(strategy, game_state)
        network.send_moving(moving)
