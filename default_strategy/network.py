import socket
import struct
from .serializer import parse_game_state, serialize_moving

class Network(object):
    def __init__(self, port):
        self.sock = socket.socket()
        self.sock.connect(('127.0.0.1', port))

    def get_game_state(self):
        data_len = struct.unpack('H', self.sock.recv(2))[0]
        data = self.sock.recv(data_len)
        game_state = parse_game_state(data)
        return game_state

    def send_moving(self, moving):
        data = serialize_moving(moving)
        data_len = len(data)

        self.sock.send(struct.pack('H', data_len))
        self.sock.send(data)
