try:
    from ..domains.domains_pb2 import World, GameState, Moving, UP, DOWN, \
        LEFT, RIGHT, GAIN_SCORE, Log
except (ImportError, ValueError):
    from domains.domains_pb2 import World, GameState, Moving, Moving, UP, \
        DOWN, LEFT, RIGHT, GAIN_SCORE, Log

class Mechanic(object):
    def __init__(self):
        self.world = World()
        self.world.tick = 0
        self.world.max_ticks = 200
        self.log = Log()

    def update(self, moving_data, player_id):
        moving = Moving()
        moving.ParseFromString(moving_data)

        player = self.get_player_by_id(player_id)
        if moving.action == LEFT:
            player.x -= 1
        if moving.action == RIGHT:
            player.x += 1
        if moving.action == UP:
            player.y -= 1
        if moving.action == DOWN:
            player.y += 1
        if moving.action == GAIN_SCORE:
            player.score += 1

    def get_game_state_data(self, for_player_id):
        game_state = GameState()
        game_state.world.CopyFrom(self.world)
        game_state.me.CopyFrom(self.get_player_by_id(for_player_id))
        return game_state.SerializeToString()

    def complete_world_updating(self):
        self.world.tick += 1
        tick = self.log.ticks.add()
        tick.CopyFrom(self.world)

    def is_game_over(self):
        return self.world.tick == self.world.max_ticks

    def get_log_data(self):
        return self.log.SerializeToString()

    def get_result(self):
        result = []
        for player in self.world.players:
            result.append({
                'player_id': player.id,
                'score': player.score
            })
        return result

    def get_world(self):
        return self.world

    def get_player_by_id(self, id):
        for player in self.world.players:
            if player.id == id:
                return player
        raise Exception("There is not boat with id " + str(id))


    def add_strategies(self, strategies):
        for strategy in strategies:
            player = self.world.players.add()
            player.id = strategy.player_id
            player.x = 4
            player.y = 7
            player.score = 0
