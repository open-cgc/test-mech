import sys
sys.path.append('..')
from game.mechanic import Mechanic
from domains.domains_pb2 import Moving, LEFT, RIGHT, UP, DOWN, GAIN_SCORE

import unittest

class FakeStrategy(object):
    def __init__(self, player_id):
        self.player_id = player_id

class TestStringMethods(unittest.TestCase):
    PLAYER_ID = 1

    def setUp(self):
        self.mech = Mechanic()
        self.mech.add_strategies([FakeStrategy(self.PLAYER_ID)])
        self.moving = Moving()

    def test_defaults(self):
        player = self.mech.get_player_by_id(self.PLAYER_ID)
        self.assertEqual(player.x, 4)
        self.assertEqual(player.y, 7)

    def test_up(self):
        self.moving.action = UP
        self.mech.update(self.moving.SerializeToString(), self.PLAYER_ID)

    def test_down(self):
        self.moving.action = DOWN
        self.mech.update(self.moving.SerializeToString(), self.PLAYER_ID)

    def test_left(self):
        self.moving.action = LEFT
        self.mech.update(self.moving.SerializeToString(), self.PLAYER_ID)

    def test_right(self):
        self.moving.action = RIGHT
        self.mech.update(self.moving.SerializeToString(), self.PLAYER_ID)

if __name__ == '__main__':
    unittest.main()
