# Test Mech
proof-of-work

## Running tests
```shell
  python3 -m tests.moving_test
```

## Running strategy
```shell
  cd ..
  python3 -m test_mech.run_strategy --port 40262
```
