from ..domains.domains_pb2 import LEFT, RIGHT, UP, DOWN, GAIN_SCORE


class Strategy(object):
    def move(self, me, world, moving):
        if world.tick % 3 == 0:
            moving.action = GAIN_SCORE
        elif world.tick % 3 == 1:
            moving.action = RIGHT
        else:
            moving.action = DOWN
